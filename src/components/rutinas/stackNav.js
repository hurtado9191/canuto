import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import RutinaGenerica from './pantallasXrutina/rutinaGenerica';
import Rutinas from './Rutinas';

const Stack = createStackNavigator();

export default function RutinaStack() {
// El name que le des al Stack.Screen debe ser el mismo que llames en la pantalla al manejar el onPress
    return (
      <Stack.Navigator>
        <Stack.Screen name="Rutinas" component={Rutinas} />
        <Stack.Screen name="Generica" component={RutinaGenerica} />
      </Stack.Navigator>
    );
}
  