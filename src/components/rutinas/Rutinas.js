import React from 'react';
import { ScrollView,
    View,
    Image, 
    Text, 
    Button,
    ImageBackground} from 'react-native';
import {style} from './rutinas.style';

export default function Rutinas({navigation}) {
    return (
        <ImageBackground style = {style.page}>
            <Text h1 style={style.h1}>All Routines</Text>
            <ScrollView style={style.cajaDeRutinas}>
                <View style= {style.cajaRutina}>
                    <Button stlyle={style.boton} title='Cardio' onPress={() => navigation.navigate('Generica')}/>
                </View>
                <View style= {style.cajaRutina}>
                    <Button stlyle={style.boton} title='Weigths' onPress={() => navigation.navigate('Generica')}/>
                </View>
                <View style= {style.cajaRutina}>
                    <Button title='Body Weight' onPress={() => navigation.navigate('Generica')}/>
                </View>
                <View style= {style.cajaRutina}>
                    <Button title='Hybrid' onPress={() => navigation.navigate('Generica')}/>
                </View>
            </ScrollView>
           
        </ImageBackground>
    )
    
}
