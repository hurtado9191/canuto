import React from 'react';
import { View } from 'react-native';
import {style} from './rutinaGenerica.style';
import RutinaBox  from '../../componentesGenericos/RutinaBox/RutinaBox';
import { rutina1, rutina2, rutina3, rutina4, rutina5 } from './data';

export default function RutinaGenerica({ navigation }) {
        return (
            <View style={style.page}>
                <RutinaBox datos={rutina1} />
                <RutinaBox datos={rutina2} />
                <RutinaBox datos={rutina3} />
                <RutinaBox datos={rutina4} />
                <RutinaBox datos={rutina5} />
            </View>
        )
}
