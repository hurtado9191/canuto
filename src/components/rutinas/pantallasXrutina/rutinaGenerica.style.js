import { StyleSheet } from 'react-native';


export const style = new StyleSheet.create({
    page : {
        //: 5,
        //: 'purple',
        flexDirection: 'column',
        alignSelf: "center", 
        marginTop: 5,
        justifyContent: 'space-around',
        alignItems: 'center',
        width: '100%',
        height:'100%',
    },
    rutinaBox: {
        justifyContent: 'space-around',
        flexDirection:'row',
        borderWidth:5,
        width:250,
        height:100,
    },
    imagen: {
        width: 75,
        height: 75,
    },
    datosBox: {
        flexDirection: 'column',
        alignContent: 'flex-end',
    }
});