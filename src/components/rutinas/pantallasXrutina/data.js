export const rutina1 = {
    titulo: 'Dragon',
    musculo: 'Pierna',
    dificultad: 'Intermedio',
    tiempo: '15 min',
}
export const rutina2 = {
    titulo: 'Andromeda',
    musculo: 'pecho',
    dificultad: 'Veterano',
    tiempo: '15 min',
}
export const rutina3 = {
    titulo: 'Pegaso',
    musculo: 'Tricep',
    dificultad: 'Novato',
    tiempo: '15 min',
}
export const rutina4 = {
        titulo: 'Fenix',
        musculo: 'pecho',
        dificultad: 'Veterano',
        tiempo: '15 min',
}
export const rutina5 = {
    titulo: 'Cisne',
    musculo: 'Bicep',
    dificultad: 'Novato',
    tiempo: '30 min',
}