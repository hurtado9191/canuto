import { StyleSheet } from 'react-native';

export const style = new StyleSheet.create({
    page : {
        alignSelf: "center", 
        marginTop: 5,
        justifyContent: 'space-around',
        alignContent: 'space-around',
        width: '100%',
        height:'100%',
        alignItems: 'center',
    },
    barra: {
        bottom: 0,
        alignSelf: 'stretch', 
        backgroundColor: 'grey', 
        justifyContent: 'center',
        flexDirection: 'row',
    },
    image: {
        marginRight: 5,
        marginLeft: 10,
        width:50,
        height: 50,
    },
    h1: {
        fontSize: 40, 
    },
    cajaRutina: {
        fontSize:15,
        borderWidth:5,
        borderColor: 'black',
        height: 75,
        width: 300,
        justifyContent: 'center',
        alignItems:'center',
        marginTop: 20,
    },
});