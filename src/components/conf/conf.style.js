import { StyleSheet } from 'react-native';


export const style = new StyleSheet.create({
    page : {
        //: 5,
        //: 'purple',
        alignSelf: "center", 
        marginTop: 5,
        justifyContent: 'space-around',
        alignContent: 'space-around',
        width: '100%',
        height:'100%',
    },
    button:{
        backgroundColor:'black',
        borderWidth:3,
        width:150,
        marginBottom:5,
    },
});