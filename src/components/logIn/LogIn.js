import React from 'react';
import { TouchableWithoutFeedback, 
    Keyboard,
    View,
    Image, 
    TextInput, 
    StyleSheet, 
    KeyboardAvoidingView, 
    Button} from 'react-native';

export default function LogIn({ navigation }) {

    return (
        <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false} >
            <View style={styles.container}>
              <Image source={require('../../../assets/logoNegro.png')}
              style={{width: 200, height: 200}}/>
              <KeyboardAvoidingView behavior='padding'>
                <TextInput
                    style= {styles.TextInput}
                    placeholder="User"
                />
                <TextInput
                    secureTextEntry={true}
                    style= {styles.TextInput}
                    placeholder="Password"
                />
              </KeyboardAvoidingView>
              <View style={styles.button}>
              <Button color='white' title='Log In' onPress={() => navigation.navigate('routeTwo')}/>
              </View>
              <Button title= 'Create Account' onPress={() => navigation.navigate('logUp')}/>
            </View>
        </TouchableWithoutFeedback>
    )
} 


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom:250
  },
  TextInput:{
    height: 40,
    width:250,
    borderColor: 'black',
    borderWidth: 1,
    marginBottom: 20,
  },
  button:{
      backgroundColor:'black',
      borderWidth:3,
      width:150,
      marginBottom:5,
  },
});
