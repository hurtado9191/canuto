import React from 'react';
import { ScrollView,
    View,
    Text,
    Image} from 'react-native';
import { style } from './blibioteca.style';
import RutinaBox  from '../componentesGenericos/RutinaBox/RutinaBox';

export default function Biblioteca() {
    return (
        <View style={style.page}>
            <Text style= {style.headText}>Mis Rutinas</Text>
            <ScrollView style={style.scroll}>
                <RutinaBox datos={datos}/>
            </ScrollView>
        </View>
    );
}

const datos = {
    titulo: 'Fav',
    musculo: 'Esta',
    dificultad: 'Porn Star',
    tiempo: 'Lo que dure',
}