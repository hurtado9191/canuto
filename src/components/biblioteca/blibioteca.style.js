import { StyleSheet } from 'react-native';


export const style = new StyleSheet.create({
    page : {
        marginTop: 20,
        //: 'purple',
        flexDirection:'column',
        alignSelf: "center", 
        alignItems: 'center',
        width: '100%',
        height:'100%',
    },
    rutinaBox: {
        justifyContent: 'space-around',
        flexDirection:'row',
        borderWidth:5,
        width:250,
        height:100,
        marginTop:20
    },
    imagen: {
        width: 75,
        height: 75,
    },
    datosBox: {
        flexDirection: 'column',
        alignContent: 'flex-end',
    },
    scroll: {
        marginTop: 25,
    }, 
    headText: {
        fontSize: 25,
    }
});