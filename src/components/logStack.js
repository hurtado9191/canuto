import { createStackNavigator } from '@react-navigation/stack';
import LogIn from './logIn/LogIn';
import CreateAccount from './createAccount/createAccount';

const Stack = createStackNavigator();
// TODO: implementar estack para navegar del log in a create account 
export default function LogStack() {
// El name que le des al Stack.Screen debe ser el mismo que llames en la pantalla al manejar el onPress
    return (
      <Stack.Navigator>
        <Stack.Screen name="LogIn" component={LogIn} />
        <Stack.Screen name="LogUp" component={CreateAccount} />
      </Stack.Navigator>
    );
}
  