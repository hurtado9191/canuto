import React from 'react';
import HomePage from './homePage/HomePage';
import { Ionicons } from '@expo/vector-icons';
import { NavigationContainer } from '@react-navigation/native';
import Biblioteca from './biblioteca/biblioteca';
import Conf from './conf/conf';
import Perfil from './perfil/perfil';
import RutinaStack from './rutinas/stackNav';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

let Tab = createBottomTabNavigator();

export default function TabNav({ navigation }) {
  
    return (
        <NavigationContainer>
          <Tab.Navigator
          screenOptions={({ route }) => ({
            tabBarIcon: ({ focused, color, size }) => {
              let iconName;
    
              if (route.name === 'Home Page') {
                iconName =  'ios-home';
              } else if (route.name === 'Rutinas') {
                iconName = 'ios-rocket';
              } else if (route.name === 'Perfil') {
                iconName = 'ios-person';
              } else if (route.name === 'Configuracion') {
                iconName = 'ios-settings';
              } else if (route.name === 'Biblioteca') {
                iconName = 'ios-book';
              } 
    
              // You can return any component that you like here!
              return <Ionicons name={iconName} size={size} color={color} />;
            },
          })}
          tabBarOptions={{
            activeTintColor: 'blue',
            inactiveTintColor: 'gray',
          }}>
            <Tab.Screen name='Biblioteca' component={Biblioteca}/>
            <Tab.Screen name='Perfil' component={Perfil} />
            <Tab.Screen name='Home Page' component={HomePage} />
            <Tab.Screen name='Rutinas' component={RutinaStack} />
            <Tab.Screen name='Configuracion' component={Conf}/>
          </Tab.Navigator>
        </NavigationContainer>
    
    );
  }
  