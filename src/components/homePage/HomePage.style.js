import { StyleSheet } from 'react-native';

export const style = new StyleSheet.create({
    page : {
        //: 5,
        //: 'purple',
        alignSelf: "center", 
        marginTop: 5,
        justifyContent: 'space-around',
        alignContent: 'space-around',
        width: '100%',
        height:'100%',
    },
    barra: {
        bottom: 0,
        alignSelf: 'stretch', 
        backgroundColor: 'grey', 
        justifyContent: 'center',
        flexDirection: 'row',
   
    },
    image: {
        marginRight: 5,
        marginLeft: 10,
        width:50,
        height: 50,
    },
    cajaStandard: {
        flexDirection: 'row',
        //borderWidth: 5,
        //borderColor: 'red', 
        alignSelf: 'stretch',
        justifyContent: 'space-evenly',
        marginTop:5,
        marginBottom:5,
    },
    imageCaja: {
        // //: 5,
        //:'blue',
        flexDirection: 'column',
        width:  170,
        height: 170,
        justifyContent: 'flex-start',

        // flex: 1,
    },
    imageText: {
        color: 'grey',
        fontSize:30,
        //backgroundColor:'black',
        shadowColor: "black",
        shadowOffset: {
            width: 5,
            height: 12,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.00,

        elevation: 24,
        width:'100%',
    },
    warmUpIm: {
        flex:2,
        fontStyle: 'italic',
        // marginLeft:10,
    },
    howToEx: {
        flex:1,
        // marginRight:10,
    },
    scroll: {
        marginTop:15,
        width: '100%',
        height: '100%',
    },
});