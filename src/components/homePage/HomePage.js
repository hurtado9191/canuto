import React from 'react';
import { ImageBackground,
    ScrollView,
    View,
    Text} from 'react-native';
import { style } from './HomePage.style';

export default function HomePage() {
    return (
        <View style = {style.page}>
            <ScrollView style= {style.scroll}> 
                <View style={[style.cajaStandard]}>
                    <ImageBackground source={require('../../../assets/fotos/warmUpCut.jpg')} style={[style.imageCaja, style.warmUpIm]} >
                        <Text style={[style.imageText]}>Warm Up</Text>
                    </ImageBackground>
                    <ImageBackground source={require('../../../assets/fotos/otra_volando.jpg')} style={[style.imageCaja, style.howToEx]} >
                        <Text style={style.imageText}>How To Exercise</Text>
                    </ImageBackground>
                </View>
                <View style={style.cajaStandard}>
                    <ImageBackground source={require('../../../assets/fotos/new_skills.jpg')} style={style.imageCaja} >
                        <Text style={style.imageText}>New Skills</Text>
                    </ImageBackground>
                    <ImageBackground source={require('../../../assets/fotos/bicicleta_cut.jpg')} style={style.imageCaja} >
                        <Text style={style.imageText}>Cardio Routines</Text>
                    </ImageBackground>
                </View>
                <View style={style.cajaStandard}>
                                       <ImageBackground source={require('../../../assets/fotos/otra_barra.jpg')} style={style.imageCaja} >
                        <Text style={style.imageText}>Daily Trainig</Text>
                    </ImageBackground>
                                       <ImageBackground source={require('../../../assets/fotos/de_manos_2weyes.jpg')} style={style.imageCaja} >
                        <Text style={style.imageText}>Seek Motivation</Text>
                    </ImageBackground>
                </View>
                <View style={style.cajaStandard}>
                                       <ImageBackground source={require('../../../assets/fotos/completa_barra_de_cabeza.jpg')} style={style.imageCaja} >
                        <Text style={style.imageText}>Challenges</Text>
                    </ImageBackground>
                                       <ImageBackground source={require('../../../assets/logoNegro.png')} style={style.imageCaja} >
                        <Text style={style.imageText}>About CANUTO</Text>
                    </ImageBackground>
                </View>
            </ScrollView>

        </View>
    )


    
}




/* barra 
<View style= {style.barra}>
<Image source={require('../../../assets/icons/bookMarkIcon.png')} style={style.image}/>
<Image source={require('../../../assets/icons/userIcon.png')} style={style.image}/>
<Image source={require('../../../assets/icons/houseIcon.png')} style={style.image}/>
<Image source={require('../../../assets/icons/stackIcon.png')} style={style.image}/>
<Image source={require('../../../assets/icons/confIcon.png')} style={style.image}/>
</View>*/ 