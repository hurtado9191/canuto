import React from 'react';
import { ImageBackground,
    ScrollView,
    View,
    Text,
    Button} from 'react-native';
import { style } from './perfil.style';

export default function Perfil() {
    return (
        <View  style={style.page}>
            <Text style={style.header}>Perfil</Text>
            <View>
                <Text>Usuario</Text>
                <Text>Correo</Text>
            </View>
            <View style={style.button}>
                <Button title='Log Out' onPress={() => navigation.navigate('LogIn')}/>
            </View>
        </View>
    );
}