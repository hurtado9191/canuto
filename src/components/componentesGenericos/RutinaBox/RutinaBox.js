import React from 'react';
import { View,
    Image, 
    Text} from 'react-native';
import { style } from './RutinaBox.style';
let data;
export default function RutinaBox( {datos} ) {
data = datos;
    return(
    <View style={style.rutinaBox}>
        <Image source={require('../../../../assets/logoNegro.png')} style={style.imagen}/>
        <Text>{data.titulo}</Text>
        <View style={style.datosBox}>
            <Text>{data.musculo}</Text>
            <Text>{data.dificultad}</Text>
            <Text>{data.tiempo}</Text>
        </View>
    </View>
    )
}

