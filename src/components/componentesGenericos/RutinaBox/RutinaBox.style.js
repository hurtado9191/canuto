import { StyleSheet } from 'react-native';

export const style = new StyleSheet.create({
    rutinaBox: {
        justifyContent: 'space-around',
        flexDirection:'row',
        borderWidth:5,
        width:250,
        height:100,
    },
    imagen: {
        width: 75,
        height: 75,
    },
    datosBox: {
        flexDirection: 'column',
        alignContent: 'flex-end',
    }
})