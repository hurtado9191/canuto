import React from 'react';
import  LogIn  from './src/components/logIn/LogIn';
import TabNav from './src/components/TabNav';
import { createSwitchNavigator, createAppContainer } from 'react-navigation';


export default function App() {
  return (
    <AppNav />
  )
}

const MySwitchNavigator = createSwitchNavigator(
{
  routeOne: LogIn,
  routeTwo: TabNav,
},
{
  initialRouteName: 'routeOne',
},
);

const AppNav = createAppContainer(MySwitchNavigator);

